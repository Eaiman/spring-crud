package com.bjit.training.transaction.service;

import org.springframework.transaction.annotation.Transactional;

import com.bjit.training.transaction.dao.CustomerDAO;
import com.bjit.training.transaction.model.Customer;

public class CustomerServiceImpl implements CustomerService {
	private CustomerDAO customerDAO;
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}
	
	@Transactional
	public void createCustomer(Customer customer) {
		customerDAO.create(customer);
	}

}
