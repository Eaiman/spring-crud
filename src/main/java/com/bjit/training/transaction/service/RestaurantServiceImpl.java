package com.bjit.training.transaction.service;

import org.springframework.transaction.annotation.Transactional;

import com.bjit.training.transaction.dao.RestaurantDAO;
import com.bjit.training.transaction.model.Restaurant;

public class RestaurantServiceImpl implements RestaurantService {
	private RestaurantDAO restaurantDAO;

	public RestaurantDAO getRestaurantDAO() {
		return restaurantDAO;
	}

	public void setRestaurantDAO(RestaurantDAO restaurantDAO) {
		this.restaurantDAO = restaurantDAO;
	}

	@Transactional
	public void createRestaurant(Restaurant restaurant) {
		restaurantDAO.create(restaurant);

	}

	@Transactional
	public void showRestaurant(Restaurant restaurant) {
		// TODO Auto-generated method stub
		restaurantDAO.show(restaurant);

	}

	public void deleteRestaurant(Restaurant restaurant) {
		// TODO Auto-generated method stub
		restaurantDAO.delete(restaurant);

	}

	public void updateRestaurant(Restaurant restaurant, int id) {
		// TODO Auto-generated method stub
		restaurantDAO.update(restaurant, 10);
		
	}

}
