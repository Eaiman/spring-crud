package com.bjit.training.transaction.service;

import com.bjit.training.transaction.model.Restaurant;

public interface RestaurantService {
	public void createRestaurant(Restaurant retaurant);

	public void showRestaurant(Restaurant retaurant);
	
	public void deleteRestaurant(Restaurant restaurant);

	public void updateRestaurant(Restaurant cust, int id);

}
