package com.bjit.training.transaction.service;

import com.bjit.training.transaction.model.Customer;

public interface CustomerService {
	public void createCustomer(Customer customer);

}
