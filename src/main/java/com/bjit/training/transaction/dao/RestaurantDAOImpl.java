package com.bjit.training.transaction.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.bjit.training.transaction.model.Food;
import com.bjit.training.transaction.model.Restaurant;

public class RestaurantDAOImpl implements RestaurantDAO {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void create(final Restaurant restaurant) {
		final String queryRestaurant = "insert into restaurant (name, address, rank) values (?,?,?)";
		String queryFood = "insert into food (name,cost,restaurant_id) values (?,?,?)";
		//

		
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		KeyHolder keyHolder = new GeneratedKeyHolder();

//		jdbcTemplate.update(queryRestaurant, new Object[] {restaurant.getName(),
//				restaurant.getAddress(), restaurant.getRank() });
		
		jdbcTemplate.update(
			    new PreparedStatementCreator() {
			    	public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
			            PreparedStatement ps =
			                connection.prepareStatement(queryRestaurant, new String[] {"id"});
			            ps.setString(1, restaurant.getName());
			            ps.setString(2, restaurant.getAddress());
			            ps.setInt(3, restaurant.getRank());
			            return ps;
			        }
			    },
			    keyHolder);
		System.out.println("Inserted into Restaurant Table Successfully "+keyHolder.getKey());

		for(Food food : restaurant.getFoods()) {
			jdbcTemplate.update(queryFood, new Object[] {food.getName(),
			     		food.getCost(), keyHolder.getKey().intValue() });
		}
		System.out.println("Inserted into Food Table Successfully");

	}

	public void show(Restaurant restaurant) {
		// TODO Auto-generated method stub

		String showRestaurantFood = "select food.name from food join restaurant where restaurant.id = food.restaurant_id ";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// System.out(jdbcTemplate.execute(showRestaurantFood));
		// System.out.println("Done");

		List<Map<String, Object>> rsaurants = jdbcTemplate.queryForList(showRestaurantFood);

		if (rsaurants != null && !rsaurants.isEmpty()) {

			for (Map<String, Object> r : rsaurants) {

				for (Iterator<Map.Entry<String, Object>> it = r.entrySet().iterator(); it.hasNext();) {

					Map.Entry<String, Object> entry = it.next();

					String key = entry.getKey();

					Object value = entry.getValue();
					System.out.println(key + " = " + value);
				}

				System.out.println();

			}

		}

	}

	public void delete(Restaurant restaurant) {
		String deleteQuery = "delete from food where id=2";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(deleteQuery);
		System.out.println("after deletion the foods are");
	

		
	}

	public void update(Restaurant restaurant, int id) {
		// TODO Auto-generated method stub
		String updateRestaurant = "UPDATE `restaurant` SET `name` = 'name changed', "
				+ "`address` = 'location also changed', `rank` = '1' WHERE `restaurant`.`id` = "+id;
	JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);	
		jdbcTemplate.update(updateRestaurant);
		System.out.println("after update the restaurants are");
		System.out.println("---------------------------------------");
		
	}


}
