package com.bjit.training.transaction.dao;

import com.bjit.training.transaction.model.Food;

public interface FoodDAO {
	public void create (Food food);
}
