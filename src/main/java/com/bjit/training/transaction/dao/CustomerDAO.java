package com.bjit.training.transaction.dao;
import com.bjit.training.transaction.model.Customer;

public interface CustomerDAO {

	public void create(Customer customer);
}