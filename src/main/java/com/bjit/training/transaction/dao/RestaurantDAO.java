package com.bjit.training.transaction.dao;

import com.bjit.training.transaction.model.Restaurant;

public interface RestaurantDAO {
	public void create (Restaurant restaurant);

	public void show(Restaurant restaurant);

	

	public void delete(Restaurant restaurant);

	public void update(Restaurant restaurant, int id);

}
