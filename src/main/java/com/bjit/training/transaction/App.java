package com.bjit.training.transaction;

import java.util.Random;
import java.util.Scanner;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.bjit.training.transaction.model.Address;
import com.bjit.training.transaction.model.Customer;
import com.bjit.training.transaction.model.Food;
import com.bjit.training.transaction.model.Restaurant;
import com.bjit.training.transaction.service.CustomerService;
import com.bjit.training.transaction.service.CustomerServiceImpl;
import com.bjit.training.transaction.service.RestaurantService;
import com.bjit.training.transaction.service.RestaurantServiceImpl;

public class App {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		RestaurantService restaurantManager = ctx.getBean("restaurantManager", RestaurantServiceImpl.class);
		Restaurant cust = createDummyRestaurant();
		restaurantManager.createRestaurant(cust);
		Restaurant show;
		restaurantManager.showRestaurant(cust);
		restaurantManager.deleteRestaurant(cust);
		restaurantManager.showRestaurant(cust);
		restaurantManager.updateRestaurant(cust, 10);
//		restaurantManager.showRestaurant(cust);
		ctx.close();
	}

	private static Restaurant createDummyRestaurant() {
//		// TODO Auto-generated method stub
//		Scanner scanner = new Scanner(System.in);
//		System.out.println("How may");
		Restaurant restaurant = new Restaurant();
//		int rand = new Random().nextInt(1000);
//		restaurant.setId(rand);
		restaurant.setName("xyz");
		restaurant.setAddress("location xyz");
		restaurant.setRank(1);
	
		Food food = new Food();
		
		food.setCost(200);
//		food.setId(new Random().nextInt(100));
		food.setName("food 1 for xyz");
		restaurant.setFood(food);
		
		food = new Food();
		food.setCost(500);
		food.setName("food 2 for xyz ");
		restaurant.setFood(food);
		
		food = new Food();
		food.setCost(50);
		food.setName("food 3 for xyz ");
		restaurant.setFood(food);

		return restaurant;
	}
}

// public class App {
// public static void main(String[] args) {
// ClassPathXmlApplicationContext ctx = new
// ClassPathXmlApplicationContext("spring.xml");
// CustomerService customerManager = ctx.getBean("customerManager",
// CustomerServiceImpl.class);
// Customer cust = createDummyCustomer();
// customerManager.createCustomer(cust); ctx.close(); }
//
//
// private static Customer createDummyCustomer() {
// Customer customer = new Customer();
// int rand = new Random().nextInt(1000);
// customer.setId(rand); customer.setName("BJIT Limited");
// Address address = new Address(); address.setCountry("Bangladesh"); // setting
// value more than 20 chars, so that SQLException occurs
// address.setAddress("Albany");
// //address.setAddress("mmmmm");
// customer.setAddress(address);
// return customer; } }