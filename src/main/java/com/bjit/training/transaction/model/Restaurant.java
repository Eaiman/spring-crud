package com.bjit.training.transaction.model;

import java.util.ArrayList;

public class Restaurant {
//	private int id;
	private String name;
	private String address;
	private int rank;
	private ArrayList<Food> foods = new ArrayList<Food>();
//	private Food  food;
//	
//	public Food getFood() {
//		return food;
//	}
	public void setFood(Food food) {
		this.foods.add(food);
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
//	public int getId() {
//		return id;
//	}
//	public void setId(int id) {
//		this.id = id;
//	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public ArrayList<Food> getFoods() {
		return foods;
	}

	public void setFoods(ArrayList<Food> foods) {
		this.foods = foods;
	}


}
